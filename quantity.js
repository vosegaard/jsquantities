/***************************************************************************************
 * MIT License                                                                         *
 *                                                                                     *
 * Copyright (c) 2020 Thomas Vosegaard                                                 *
 *                                                                                     *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of     *
 * this software and associated documentation files (the "Software"), to deal in the   *
 * Software without restriction, including without limitation the rights to use, copy, *
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, *
 * and to permit persons to whom the Software is furnished to do so, subject to the    *
 * following conditions:                                                               *
 *                                                                                     *
 * The above copyright notice and this permission notice shall be included in all      *
 * copies or substantial portions of the Software.                                     *
 *                                                                                     *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, *
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A       *
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT  *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION   *
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE      *
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                              *
 ***************************************************************************************/



function Quantity(str, quantity, reciprocalQuantity) {
   this.text = str;
   var u = Quantity.parseQuantity(str);
   if (!u) return false;
   this.parsed = u;
   this.value = u.value;
   this.unit = u.unit;
   this.setQuantity = quantity;
   this.setReciprocalQuantity = reciprocalQuantity;
   if (quantity) {
      this.quantity = this.isQuantity(quantity);
   } else {
      var q = this.isQuantity();
      if (q.length > 1) {
         this.quantities = q;
      } else if (q.length == 1) {
         this.quantity = q[0];
      }
   }
}

Quantity.prototype.getValue = function(si) {
   if (si) {
      return this.value;
   } else {
      return Number.parseFloat(this.text.split(" ")[0]);
   }
}

Quantity.prototype.to = function(unit) {
   var u = new Unit("1 " + unit);
   console.log(u, u.get(true));
   if (!this.unitEquals(u.unit)) {
      console.error("to: Not same dimensionality of " + this.getUnit(true) + " and requested " + unit);
      return false;
   }
   return this.value/u.value + " " + unit;
}

Quantity.prototype.reciprocal = function() {
   this.unit = this.unit.map(k => -k);
   var uu = this.getUnit(true);
   this.unit = this.unit.map(k => -k);
   var val = (1/this.value) + " " + uu;
   var u = new Unit(val, this.reciprocalQuantity, this.quantity);
   return u;
}

Quantity.prototype.isReciprocalQuantity = function(q) {
   var u = this.reciprocal();
   return u.isQuantity(q);
}

Quantity.prototype.unitEquals = function(uu) {
   for (var i=0; i<7; i++) {
      if (uu[i] != this.unit[i]) return false;
   }
   return true;
}
Quantity.prototype.reciprocalUnitEquals = function(uu) {
   for (var i=0; i<7; i++) {
      if (uu[i] != -this.unit[i]) return false;
   }
   return true;
}

Quantity.prototype.isQuantity = function(q) {
   if (q) {
      if (!Quantity.quantities[q]) {
         console.error("isQuantity: requested quantity '" + q + "' is not a valid quantity");
         return false;
      }
      var uu = Quantity.quantities[q].unit;
      return this.unitEquals(uu) ? [q]:false;
   }
   var possible = [];
   var keys = Object.keys(Quantity.quantities);
   for (var j=0; j<keys.length; j++) {
      var qq = Quantity.quantities[keys[j]];
      if (this.unitEquals(qq.unit)) possible.push(keys[j]);
   }
   if (possible.length == 0) return false;
   return possible;
}

Quantity.prototype.get = function(si) {
   if (si) {
      return this.value + " " + this.getUnit(true);
   } else {
      return this.text;
   }
}

Quantity.prototype.getUnit = function(si) {
   if (si) {
      var pos = [];
      var neg = [];
      
      var dist = Quantity.preferredUnits.map(k => {
         var dst = 0;
         for (var i=0; i<7; i++) dst += Math.abs(this.unit[i]-Quantity.unitsConstants[k].unit[i]);
         return {unit: k, dist: dst};
      }).sort((a,b) => a.dist-b.dist);
      
      if (dist[0].dist == 0) {
         return dist[0].unit;
      }
      var uu = Object.keys(Quantity.baseUnits);
      for (var i=0; i<7; i++) {
         if (this.unit[i] > 0) {
            var str = uu[i];
            if (this.unit[i] > 1) str += "^" + this.unit[i];
            pos.push(str);
         }
         if (this.unit[i] < 0) {
            var str = uu[i];
            if (this.unit[i] < -1) str += "^" + (-this.unit[i]);
            neg.push(str);
         }
      }
      str = "1";
      if (pos.length > 0) {
         str = pos.join("*");
      }
      if (neg.length > 1) {
         str += "/(" + neg.join("*") + ")";
      } else if (neg.length == 1) {
         str += "/" + neg[0];
      }
      return str;
   } else {
      return this.text.split(" ")[1];
   }
}

Quantity.getUnit = function(str) {
   var i = 1;
   var c = str.charAt(1);
   while (i < str.length && c != "(" && c != "^" && c != "*" && c != "/" && c != ")") {
      i++;
      if (i < str.length) c = str.charAt(i)
   }
   var u = str.substr(0, i);
   var uu = false;
   var pre = 0;
   var val = 1;
   if (Quantity.baseUnits[u]) {
      //console.log("Found base unit " + u);
      uu = Quantity.baseUnits[u];
   } else if (Quantity.quantities[u]) {
      //console.log("Found quantity " + u);
      uu = Quantity.quantities[u];
   } else if (Quantity.unitsConstants[u]) {
      //console.log("Found constant " + u);
      uu = Quantity.unitsConstants[u];
      val = 1*uu.si.split(" ")[0];
   } else {
      var check = false;
      var p = u.charAt(0);
      if (u.substr(0,2) == "da") {
         pre = Quantity.prefixes.da;
         check = u.substr(2);
      } else if (Quantity.prefixes[p]) {
         pre = Quantity.prefixes[p];
         check = u.substr(1);
      }
      if (check) {
         if (Quantity.baseUnits[check]) {
            //console.log("Found base unit " + check);
            uu = Quantity.baseUnits[check];
            val = Math.pow(10, pre);
         } else if (Quantity.quantities[check]) {
            //console.log("Found quantity " + check);
            uu = Quantity.quantities[check]
            val = Math.pow(10, pre);
         } else if (Quantity.unitsConstants[check]) {
            //console.log("Found constant " + check);
            uu = Quantity.unitsConstants[check]
            val = 1*uu.si.split(" ")[0]*Math.pow(10, pre);
         }
      }
   }
   if (!uu) {
      console.error("Failed to interpret " + str);
      return false;
   }
   console.log("Returning unit " + u + " " + val + " " + pre)
   return {txt: u, unit: uu.unit, value: val, str: str.substr(i)}
}

Quantity.getBracket = function(str) {
   var p = 1;
   var i = 1;
   while (p > 0 && i < str.length) {
      if (str.charAt(i) == ")") p--;
      if (str.charAt(i) == "(") p++;
      i++;
   }
   if (p > 0) {
      console.error("Misbalanced parentheses in '" + str + "'");
   }
   return {unit: str.substr(1, i-2), str: str.substr(i)}
}
Quantity.parseQuantity = function(str) {
   //console.log(str);
   var ary = str.replace(/  /g, " ").replace(/  /g, " ").split(" ");
   if (ary.length == 1) {
      // Dimensionless quantity
      if (Number.isNaN(Number.parseFloat(ary[0]))) return false;
      return {value: Number.parseFloat(ary[0]), unit: [0,0,0,0,0,0,0]}
   } else {
      var value = 1*ary[0];
      var unit = ary[1];
      if (ary.length > 2) {
         var unit = ary.splice(1).join("");
      }
      //console.log("parsing " + unit)
      var unit = Quantity.parseUnit(unit);
      if (!unit) return false;
      //console.log(unit, Number.parseFloat(ary[0]), unit.value);
      return {value: Number.parseFloat(ary[0])*unit.value, unit: unit.unit, unitstr: ary[1]}
   }
}

Quantity.parseUnit = function(str) {
   var sign = 1;
   var unit = [0,0,0,0,0,0,0];
   var prefix = 0;
   var num = 1;
   var val = 1, value = 1;
   while (str) {
      var c = str.charAt(0);
      if (c != "^" && uu) {
         //console.log("Adding " + uu);
         for (var i=0; i<7; i++) unit[i] += sign*num*uu[i];
         uu = false;
         //console.log("temp val " + val);
         value *= Math.pow(val, sign*num);
         val = 1;
      }
      if (c == "(") {
         num = 1;
         var res = Quantity.getBracket(str);
         console.log("Bracket ", res)
         ur = Quantity.parseUnit(res.unit);
         console.log("Parsed unit: ", ur);
         uu = ur.unit;
         val = ur.value;
         //console.log("Bracket content " + str + uu)
      } else if (c == "*") {
         var res = {str: str.substr(1)}
         num = 1;
         sign = 1;
      } else if (c == "/") {
         var res = {str: str.substr(1)}
         num = 1;
         var sign = -1;
      } else if (c == "^") {
         s = str.substr(1);
         var sadd = 0;
         if (str.charAt(1) == "(") {
            var r = Quantity.getBracket(str.substr(1));
            var s = r.unit;
            sadd = 2;
         }
         var i = 0;
         while (s.charAt(i).match(/[0-9eE\.\-]/) && i < s.length) i++;
         num = s.substr(0,i);
         //console.log("Yeah " + num + " " + i);
         var res = {str: str.substr(i+sadd+1)}
      } else if (c == 1) {
         num = 1;
         res = {str: str.substr(1)}
      } else {
         // Unit is starting here
         var res = Quantity.getUnit(str);
         if (!res) {
            return false;
         }
         num = 1;
         console.log("Unit", res);
         var uu = res.unit;
         val = res.value;
      }
      str = res.str;
   }
   if (uu) {
      for (var i=0; i<7; i++) unit[i] += sign*num*uu[i];
      //console.log("temp val " + val);
      value *= Math.pow(val, sign*num);
   }
   //console.log("Parse unit returns " + unit)
   return {unit: unit, prefix: prefix, value: value}
}




Quantity.prefixes = {
   Y: 24,
   Z: 21,
   E: 18,
   P: 15,
   T: 12,
   G: 9,
   M: 6,
   k: 3,
   h: 2,
   da: 1,
   d: -1,
   c: -2,
   m: -3,
   µ: -6,
   n: -9,
   p: -12,
   f: -15,
   a: -18,
   z: -21,
   y: -24
}

/*
var unitID = {
   L: 0,
   M: 1,
   T: 2,
   I: 3,
   O: 4,
   N: 5,
   J: 6
}
*/

Quantity.baseUnits = {
   "m":   {dimension: "length",                    name: "meters",      unit: [1,0,0,0,0,0,0], dimensionality: "L"},
   "kg":  {dimension: "mass",                      name: "kilograms",   unit: [0,1,0,0,0,0,0], dimensionality: "M"},
   "s":   {dimension: "time",                      name: "seconds",     unit: [0,0,1,0,0,0,0], dimensionality: "T"},
   "A":   {dimension: "electric current",          name: "ampere",      unit: [0,0,0,1,0,0,0], dimensionality: "I"},
   "K":   {dimension: "thermodynamic temperature", name: "kelvin",      unit: [0,0,0,0,1,0,0], dimensionality: "Θ"},
   "mol": {dimension: "amount of substance",       name: "moles",       unit: [0,0,0,0,0,1,0], dimensionality: "N"},
   "cd":  {dimension: "luminous intensity",        name: "candelas",    unit: [0,0,0,0,0,0,1], dimensionality: "J"}
}

Quantity.quantities = {
   "absorbed dose": {name: "absorbed dose", dimensionality: "L^2*M/(M*T^2)", unit: [2,0,-2,0,0,0,0], value: 1},
   "absorbed dose rate": {name: "absorbed dose rate", dimensionality: "L^2/T^3", unit: [2,0,-3,0,0,0,0], value: 1},
   "acceleration": {name: "acceleration", dimensionality: "L/T^2", unit: [1,0,-2,0,0,0,0], value: 1},
   "action": {name: "action", dimensionality: "L^2*M/T", unit: [2,1,-1,0,0,0,0], value: 1},
   "amount": {name: "amount", dimensionality: "N", unit: [0,0,0,0,0,1,0], value: 1},
   "amount concentration": {name: "amount concentration", dimensionality: "N/L^3", unit: [-3,0,0,0,0,1,0], value: 1},
   "amount of electricity": {name: "amount of electricity", dimensionality: "T*I", unit: [0,0,1,1,0,0,0], value: 1},
   "amount ratio": {name: "amount ratio", dimensionality: "N/N", unit: [0,0,0,0,0,0,0], value: 1},
   "angular acceleration": {name: "angular acceleration", dimensionality: "L/(L*T^2)", unit: [0,0,-2,0,0,0,0], value: 1},
   "angular frequency": {name: "angular frequency", dimensionality: "L/(L*T)", unit: [0,0,-1,0,0,0,0], value: 1},
   "angular momentum": {name: "angular momentum", dimensionality: "L^2*M/T", unit: [2,1,-1,0,0,0,0], value: 1},
   "angular speed": {name: "angular speed", dimensionality: "L/(L*T)", unit: [0,0,-1,0,0,0,0], value: 1},
   "angular velocity": {name: "angular velocity", dimensionality: "L/(L*T)", unit: [0,0,-1,0,0,0,0], value: 1},
   "area": {name: "area", dimensionality: "L^2", unit: [2,0,0,0,0,0,0], value: 1},
   "area ratio": {name: "area ratio", dimensionality: "L^2/L^2", unit: [0,0,0,0,0,0,0], value: 1},
   "capacitance": {name: "capacitance", dimensionality: "T^4*I^2/(L^2*M)", unit: [-2,-1,4,2,0,0,0], value: 1},
   "catalytic activity": {name: "catalytic activity", dimensionality: "N/T", unit: [0,0,-1,0,0,1,0], value: 1},
   "catalytic activity concentration": {name: "catalytic activity concentration", dimensionality: "N/(L^3*T)", unit: [-3,0,-1,0,0,1,0], value: 1},
   "catalytic activity content": {name: "catalytic activity content", dimensionality: "N/(M*T)", unit: [0,-1,-1,0,0,1,0], value: 1},
   "charge to amount ratio": {name: "charge to amount ratio", dimensionality: "T*I/N", unit: [0,0,1,1,0,-1,0], value: 1},
   "charge to mass ratio": {name: "charge to mass ratio", dimensionality: "T*I/M", unit: [0,-1,1,1,0,0,0], value: 1},
   "circulation": {name: "circulation", dimensionality: "L^2/T", unit: [2,0,-1,0,0,0,0], value: 1},
   "compressibility": {name: "compressibility", dimensionality: "L*T^2/M", unit: [1,-1,2,0,0,0,0], value: 1},
   "current": {name: "current", dimensionality: "I", unit: [0,0,0,1,0,0,0], value: 1},
   "current density": {name: "current density", dimensionality: "I/L^2", unit: [-2,0,0,1,0,0,0], value: 1},
   "current ratio": {name: "current ratio", dimensionality: "I/I", unit: [0,0,0,0,0,0,0], value: 1},
   "density": {name: "density", dimensionality: "M/L^3", unit: [-3,1,0,0,0,0,0], value: 1},
   "diffusion coefficient": {name: "diffusion coefficient", dimensionality: "L^2/T", unit: [2,0,-1,0,0,0,0], value: 1},
   "diffusion flux": {name: "diffusion flux", dimensionality: "N/(L^2*T)", unit: [-2,0,-1,0,0,1,0], value: 1},
   "dimensionless": {name: "dimensionless", dimensionality: "1", unit: [0,0,0,0,0,0,0], value: 1},
   "distance per volume": {name: "distance per volume", dimensionality: "L/L^3", unit: [-2,0,0,0,0,0,0], value: 1},
   "dose equivalent": {name: "dose equivalent", dimensionality: "L^2*M/(M*T^2)", unit: [2,0,-2,0,0,0,0], value: 1},
   "dynamic viscosity": {name: "dynamic viscosity", dimensionality: "M/(L*T)", unit: [-1,1,-1,0,0,0,0], value: 1},
   "elastic modulus": {name: "elastic modulus", dimensionality: "M/(L*T^2)", unit: [-1,1,-2,0,0,0,0], value: 1},
   "electric charge": {name: "electric charge", dimensionality: "T*I", unit: [0,0,1,1,0,0,0], value: 1},
   "electric charge density": {name: "electric charge density", dimensionality: "T*I/L^3", unit: [-3,0,1,1,0,0,0], value: 1},
   "electric conductance": {name: "electric conductance", dimensionality: "T^3*I^2/(L^2*M)", unit: [-2,-1,3,2,0,0,0], value: 1},
   "electric conductivity": {name: "electric conductivity", dimensionality: "T^3*I^2/(L^3*M)", unit: [-3,-1,3,2,0,0,0], value: 1},
   "electric dipole moment": {name: "electric dipole moment", dimensionality: "L*T*I", unit: [1,0,1,1,0,0,0], value: 1},
   "electric displacement": {name: "electric displacement", dimensionality: "T*I/L^2", unit: [-2,0,1,1,0,0,0], value: 1},
   "electric field gradient": {name: "electric field gradient", dimensionality: "L^2*M/(L^2*T^3*I)", unit: [0,1,-3,-1,0,0,0], value: 1},
   "electric field strength": {name: "electric field strength", dimensionality: "L*M/(T^3*I)", unit: [1,1,-3,-1,0,0,0], value: 1},
   "electric flux": {name: "electric flux", dimensionality: "L^3*M/(T*I)", unit: [3,1,-1,-1,0,0,0], value: 1},
   "electric flux density": {name: "electric flux density", dimensionality: "T*I/L^2", unit: [-2,0,1,1,0,0,0], value: 1},
   "electric polarizability": {name: "electric polarizability", dimensionality: "L^2*T^4*I^2/(L^2*M)", unit: [0,-1,4,2,0,0,0], value: 1},
   "electric potential difference": {name: "electric potential difference", dimensionality: "L^2*M/(T^3*I)", unit: [2,1,-3,-1,0,0,0], value: 1},
   "electric quadrupole moment": {name: "electric quadrupole moment", dimensionality: "L^2*T*I", unit: [2,0,1,1,0,0,0], value: 1},
   "electric resistance": {name: "electric resistance", dimensionality: "L^2*M/(T^3*I^2)", unit: [2,1,-3,-2,0,0,0], value: 1},
   "electric resistance per length": {name: "electric resistance per length", dimensionality: "L^2*M/(L*T^3*I^2)", unit: [1,1,-3,-2,0,0,0], value: 1},
   "electric resistivity": {name: "electric resistivity", dimensionality: "L^3*M/(T^3*I^2)", unit: [3,1,-3,-2,0,0,0], value: 1},
   "electrical mobility": {name: "electrical mobility", dimensionality: "L^2*T^3*I/(L^2*M*T)", unit: [0,-1,2,1,0,0,0], value: 1},
   "electromotive force": {name: "electromotive force", dimensionality: "L^2*M/(T^3*I)", unit: [2,1,-3,-1,0,0,0], value: 1},
   "energy": {name: "energy", dimensionality: "L^2*M/T^2", unit: [2,1,-2,0,0,0,0], value: 1},
   "energy density": {name: "energy density", dimensionality: "M/(L*T^2)", unit: [-1,1,-2,0,0,0,0], value: 1},
   "entropy": {name: "entropy", dimensionality: "L^2*M/(T^2*Θ)", unit: [2,1,-2,0,-1,0,0], value: 1},
   "fine structure constant": {name: "fine structure constant", dimensionality: "L^5*M*T^4*I^2/(L^5*M*T^4*I^2)", unit: [0,0,0,0,0,0,0], value: 1},
   "first hyperpolarizability": {name: "first hyperpolarizability", dimensionality: "L^3*T^7*I^3/(L^4*M^2)", unit: [-1,-2,7,3,0,0,0], value: 1},
   "fluidity": {name: "fluidity", dimensionality: "L*T/M", unit: [1,-1,1,0,0,0,0], value: 1},
   "force": {name: "force", dimensionality: "L*M/T^2", unit: [1,1,-2,0,0,0,0], value: 1},
   "frequency": {name: "frequency", dimensionality: "1/T", unit: [0,0,-1,0,0,0,0], value: 1},
   "frequency per electric field gradient": {name: "frequency per electric field gradient", dimensionality: "L^2*T^3*I/(L^2*M*T)", unit: [0,-1,2,1,0,0,0], value: 1},
   "frequency per electric field gradient squared": {name: "frequency per electric field gradient squared", dimensionality: "L^4*T^6*I^2/(L^4*M^2*T)", unit: [0,-2,5,2,0,0,0], value: 1},
   "frequency per magnetic flux density": {name: "frequency per magnetic flux density", dimensionality: "T*I/M", unit: [0,-1,1,1,0,0,0], value: 1},
   "frequency ratio": {name: "frequency ratio", dimensionality: "T/T", unit: [0,0,0,0,0,0,0], value: 1},
   "gas permeance": {name: "gas permeance", dimensionality: "L*T^2*N/(L^2*M*T)", unit: [-1,-1,1,0,0,1,0], value: 1},
   "gravitational constant": {name: "gravitational constant", dimensionality: "L^3/(M*T^2)", unit: [3,-1,-2,0,0,0,0], value: 1},
   "gyromagnetic ratio": {name: "gyromagnetic ratio", dimensionality: "L*T^2*I/(L*M*T)", unit: [0,-1,1,1,0,0,0], value: 1},
   "heat capacity": {name: "heat capacity", dimensionality: "L^2*M/(T^2*Θ)", unit: [2,1,-2,0,-1,0,0], value: 1},
   "heat flux density": {name: "heat flux density", dimensionality: "L^2*M/(L^2*T^3)", unit: [0,1,-3,0,0,0,0], value: 1},
   "heat transfer coefficient": {name: "heat transfer coefficient", dimensionality: "M/(T^3*Θ)", unit: [0,1,-3,0,-1,0,0], value: 1},
   "illuminance": {name: "illuminance", dimensionality: "L^2*J/L^4", unit: [-2,0,0,0,0,0,1], value: 1},
   "inductance": {name: "inductance", dimensionality: "L^2*M/(T^2*I^2)", unit: [2,1,-2,-2,0,0,0], value: 1},
   "inverse amount": {name: "inverse amount", dimensionality: "1/N", unit: [0,0,0,0,0,-1,0], value: 1},
   "inverse amount concentration inverse time": {name: "inverse amount concentration inverse time", dimensionality: "L^3/(T*N)", unit: [3,0,-1,0,0,-1,0], value: 1},
   "inverse area": {name: "inverse area", dimensionality: "1/L^2", unit: [-2,0,0,0,0,0,0], value: 1},
   "inverse current": {name: "inverse current", dimensionality: "1/I", unit: [0,0,0,-1,0,0,0], value: 1},
   "inverse length": {name: "inverse length", dimensionality: "1/L", unit: [-1,0,0,0,0,0,0], value: 1},
   "inverse luminous intensity": {name: "inverse luminous intensity", dimensionality: "1/J", unit: [0,0,0,0,0,0,-1], value: 1},
   "inverse magnetic flux density": {name: "inverse magnetic flux density", dimensionality: "T^2*I/M", unit: [0,-1,2,1,0,0,0], value: 1},
   "inverse mass": {name: "inverse mass", dimensionality: "1/M", unit: [0,-1,0,0,0,0,0], value: 1},
   "inverse temperature": {name: "inverse temperature", dimensionality: "1/Θ", unit: [0,0,0,0,-1,0,0], value: 1},
   "inverse time": {name: "inverse time", dimensionality: "1/T", unit: [0,0,-1,0,0,0,0], value: 1},
   "inverse time squared": {name: "inverse time squared", dimensionality: "1/T^2", unit: [0,0,-2,0,0,0,0], value: 1},
   "inverse volume": {name: "inverse volume", dimensionality: "1/L^3", unit: [-3,0,0,0,0,0,0], value: 1},
   "irradiance": {name: "irradiance", dimensionality: "L^2*M/(L^2*T^3)", unit: [0,1,-3,0,0,0,0], value: 1},
   "kinematic viscosity": {name: "kinematic viscosity", dimensionality: "L^2/T", unit: [2,0,-1,0,0,0,0], value: 1},
   "length": {name: "length", dimensionality: "L", unit: [1,0,0,0,0,0,0], value: 1},
   "length ratio": {name: "length ratio", dimensionality: "L/L", unit: [0,0,0,0,0,0,0], value: 1},
   "linear momentum": {name: "linear momentum", dimensionality: "L*M/T", unit: [1,1,-1,0,0,0,0], value: 1},
   "luminance": {name: "luminance", dimensionality: "J/L^2", unit: [-2,0,0,0,0,0,1], value: 1},
   "luminous efficacy": {name: "luminous efficacy", dimensionality: "T^3*J/(L^2*M)", unit: [-2,-1,3,0,0,0,1], value: 1},
   "luminous energy": {name: "luminous energy", dimensionality: "L^2*T*J/L^2", unit: [0,0,1,0,0,0,1], value: 1},
   "luminous flux": {name: "luminous flux", dimensionality: "L^2*J/L^2", unit: [0,0,0,0,0,0,1], value: 1},
   "luminous flux density": {name: "luminous flux density", dimensionality: "L^2*J/L^4", unit: [-2,0,0,0,0,0,1], value: 1},
   "luminous intensity": {name: "luminous intensity", dimensionality: "J", unit: [0,0,0,0,0,0,1], value: 1},
   "luminous intensity ratio": {name: "luminous intensity ratio", dimensionality: "J/J", unit: [0,0,0,0,0,0,0], value: 1},
   "magnetic dipole moment": {name: "magnetic dipole moment", dimensionality: "L^2*I", unit: [2,0,0,1,0,0,0], value: 1},
   "magnetic dipole moment ratio": {name: "magnetic dipole moment ratio", dimensionality: "L^2*I/(L^2*I)", unit: [0,0,0,0,0,0,0], value: 1},
   "magnetic field gradient": {name: "magnetic field gradient", dimensionality: "M/(L*T^2*I)", unit: [-1,1,-2,-1,0,0,0], value: 1},
   "magnetic field strength": {name: "magnetic field strength", dimensionality: "I/L", unit: [-1,0,0,1,0,0,0], value: 1},
   "magnetic flux": {name: "magnetic flux", dimensionality: "L^2*M/(T^2*I)", unit: [2,1,-2,-1,0,0,0], value: 1},
   "magnetic flux density": {name: "magnetic flux density", dimensionality: "M/(T^2*I)", unit: [0,1,-2,-1,0,0,0], value: 1},
   "magnetizability": {name: "magnetizability", dimensionality: "L^2*M*T^4*I^2/(M^2*T^2)", unit: [2,-1,2,2,0,0,0], value: 1},
   "mass": {name: "mass", dimensionality: "M", unit: [0,1,0,0,0,0,0], value: 1},
   "mass concentration": {name: "mass concentration", dimensionality: "M/L^3", unit: [-3,1,0,0,0,0,0], value: 1},
   "mass flow rate": {name: "mass flow rate", dimensionality: "M/T", unit: [0,1,-1,0,0,0,0], value: 1},
   "mass flux": {name: "mass flux", dimensionality: "M/(L^2*T)", unit: [-2,1,-1,0,0,0,0], value: 1},
   "mass ratio": {name: "mass ratio", dimensionality: "M/M", unit: [0,0,0,0,0,0,0], value: 1},
   "mass to charge ratio": {name: "mass to charge ratio", dimensionality: "M/(T*I)", unit: [0,1,-1,-1,0,0,0], value: 1},
   "molality": {name: "molality", dimensionality: "N/M", unit: [0,-1,0,0,0,1,0], value: 1},
   "molar conductivity": {name: "molar conductivity", dimensionality: "L^2*T^3*I^2/(L^2*M*N)", unit: [0,-1,3,2,0,-1,0], value: 1},
   "molar energy": {name: "molar energy", dimensionality: "L^2*M/(T^2*N)", unit: [2,1,-2,0,0,-1,0], value: 1},
   "molar entropy": {name: "molar entropy", dimensionality: "L^2*M/(T^2*Θ*N)", unit: [2,1,-2,0,-1,-1,0], value: 1},
   "molar heat capacity": {name: "molar heat capacity", dimensionality: "L^2*M/(T^2*Θ*N)", unit: [2,1,-2,0,-1,-1,0], value: 1},
   "molar magnetic susceptibility": {name: "molar magnetic susceptibility", dimensionality: "L^3/N", unit: [3,0,0,0,0,-1,0], value: 1},
   "molar mass": {name: "molar mass", dimensionality: "M/N", unit: [0,1,0,0,0,-1,0], value: 1},
   "moment of force": {name: "moment of force", dimensionality: "L^2*M^2/(M*T^2)", unit: [2,1,-2,0,0,0,0], value: 1},
   "moment of inertia": {name: "moment of inertia", dimensionality: "L^2*M", unit: [2,1,0,0,0,0,0], value: 1},
   "permeability": {name: "permeability", dimensionality: "L*M/(T^2*I^2)", unit: [1,1,-2,-2,0,0,0], value: 1},
   "permittivity": {name: "permittivity", dimensionality: "T^4*I^2/(L^3*M)", unit: [-3,-1,4,2,0,0,0], value: 1},
   "plane angle": {name: "plane angle", dimensionality: "L/L", unit: [0,0,0,0,0,0,0], value: 1},
   "porosity": {name: "porosity", dimensionality: "L^3/L^3", unit: [0,0,0,0,0,0,0], value: 1},
   "power": {name: "power", dimensionality: "L^2*M/T^3", unit: [2,1,-3,0,0,0,0], value: 1},
   "power per luminous flux": {name: "power per luminous flux", dimensionality: "L^3*M/(L*T^3*J)", unit: [2,1,-3,0,0,0,-1], value: 1},
   "pressure": {name: "pressure", dimensionality: "M/(L*T^2)", unit: [-1,1,-2,0,0,0,0], value: 1},
   "pressure gradient": {name: "pressure gradient", dimensionality: "M/(L^2*T^2)", unit: [-2,1,-2,0,0,0,0], value: 1},
   "radiance": {name: "radiance", dimensionality: "L^4*M/(L^4*T^3)", unit: [0,1,-3,0,0,0,0], value: 1},
   "radiant flux": {name: "radiant flux", dimensionality: "L^2*M/T^3", unit: [2,1,-3,0,0,0,0], value: 1},
   "radiant intensity": {name: "radiant intensity", dimensionality: "L^4*M/(L^2*T^3)", unit: [2,1,-3,0,0,0,0], value: 1},
   "radiation exposure": {name: "radiation exposure", dimensionality: "T*I/M", unit: [0,-1,1,1,0,0,0], value: 1},
   "radioactivity": {name: "radioactivity", dimensionality: "1/T", unit: [0,0,-1,0,0,0,0], value: 1},
   "reduced action": {name: "reduced action", dimensionality: "L^3*M/(L*T)", unit: [2,1,-1,0,0,0,0], value: 1},
   "refractive index": {name: "refractive index", dimensionality: "L*T/(L*T)", unit: [0,0,0,0,0,0,0], value: 1},
   "rock permeability": {name: "rock permeability", dimensionality: "L^2", unit: [2,0,0,0,0,0,0], value: 1},
   "second hyperpolarizability": {name: "second hyperpolarizability", dimensionality: "L^4*T^10*I^4/(L^6*M^3)", unit: [-2,-3,10,4,0,0,0], value: 1},
   "second radiation constant": {name: "second radiation constant", dimensionality: "L^3*M*T^2*Θ/(L^2*M*T^2)", unit: [1,0,0,0,1,0,0], value: 1},
   "solid angle": {name: "solid angle", dimensionality: "L^2/L^2", unit: [0,0,0,0,0,0,0], value: 1},
   "specific energy": {name: "specific energy", dimensionality: "L^2/T^2", unit: [2,0,-2,0,0,0,0], value: 1},
   "specific entropy": {name: "specific entropy", dimensionality: "L^2/(T^2*Θ)", unit: [2,0,-2,0,-1,0,0], value: 1},
   "specific gravity": {name: "specific gravity", dimensionality: "L^3*M/(L^3*M)", unit: [0,0,0,0,0,0,0], value: 1},
   "specific heat capacity": {name: "specific heat capacity", dimensionality: "L^2/(T^2*Θ)", unit: [2,0,-2,0,-1,0,0], value: 1},
   "specific power": {name: "specific power", dimensionality: "L^2*M/(M*T^3)", unit: [2,0,-3,0,0,0,0], value: 1},
   "specific surface area": {name: "specific surface area", dimensionality: "L^2/M", unit: [2,-1,0,0,0,0,0], value: 1},
   "specific volume": {name: "specific volume", dimensionality: "L^3/M", unit: [3,-1,0,0,0,0,0], value: 1},
   "spectral power": {name: "spectral power", dimensionality: "L^2*M/(L*T^3)", unit: [1,1,-3,0,0,0,0], value: 1},
   "spectral radiance": {name: "spectral radiance", dimensionality: "L^4*M/(L^5*T^3)", unit: [-1,1,-3,0,0,0,0], value: 1},
   "spectral radiant energy": {name: "spectral radiant energy", dimensionality: "L^2*M/(L*T^2)", unit: [1,1,-2,0,0,0,0], value: 1},
   "spectral radiant flux density": {name: "spectral radiant flux density", dimensionality: "M/(L*T^3)", unit: [-1,1,-3,0,0,0,0], value: 1},
   "spectral radiant intensity": {name: "spectral radiant intensity", dimensionality: "L^4*M/(L^3*T^3)", unit: [1,1,-3,0,0,0,0], value: 1},
   "speed": {name: "speed", dimensionality: "L/T", unit: [1,0,-1,0,0,0,0], value: 1},
   "stefan-boltzmann constant": {name: "stefan-boltzmann constant", dimensionality: "L^2*M/(L^2*T^3*Θ^4)", unit: [0,1,-3,0,-4,0,0], value: 1},
   "stress": {name: "stress", dimensionality: "M/(L*T^2)", unit: [-1,1,-2,0,0,0,0], value: 1},
   "stress-optic coefficient": {name: "stress-optic coefficient", dimensionality: "L*T^2/M", unit: [1,-1,2,0,0,0,0], value: 1},
   "surface area to volume ratio": {name: "surface area to volume ratio", dimensionality: "L^2/L^3", unit: [-1,0,0,0,0,0,0], value: 1},
   "surface charge density": {name: "surface charge density", dimensionality: "T*I/L^2", unit: [-2,0,1,1,0,0,0], value: 1},
   "surface density": {name: "surface density", dimensionality: "M/L^2", unit: [-2,1,0,0,0,0,0], value: 1},
   "surface energy": {name: "surface energy", dimensionality: "L^2*M/(L^2*T^2)", unit: [0,1,-2,0,0,0,0], value: 1},
   "surface tension": {name: "surface tension", dimensionality: "M/T^2", unit: [0,1,-2,0,0,0,0], value: 1},
   "temperature": {name: "temperature", dimensionality: "Θ", unit: [0,0,0,0,1,0,0], value: 1},
   "temperature gradient": {name: "temperature gradient", dimensionality: "Θ/L", unit: [-1,0,0,0,1,0,0], value: 1},
   "temperature ratio": {name: "temperature ratio", dimensionality: "Θ/Θ", unit: [0,0,0,0,0,0,0], value: 1},
   "thermal conductance": {name: "thermal conductance", dimensionality: "L^2*M/(T^3*Θ)", unit: [2,1,-3,0,-1,0,0], value: 1},
   "thermal conductivity": {name: "thermal conductivity", dimensionality: "L*M/(T^3*Θ)", unit: [1,1,-3,0,-1,0,0], value: 1},
   "time": {name: "time", dimensionality: "T", unit: [0,0,1,0,0,0,0], value: 1},
   "time ratio": {name: "time ratio", dimensionality: "T/T", unit: [0,0,0,0,0,0,0], value: 1},
   "torque": {name: "torque", dimensionality: "L^3*M/(L*T^2)", unit: [2,1,-2,0,0,0,0], value: 1},
   "velocity": {name: "velocity", dimensionality: "L/T", unit: [1,0,-1,0,0,0,0], value: 1},
   "voltage": {name: "voltage", dimensionality: "L^2*M/(T^3*I)", unit: [2,1,-3,-1,0,0,0], value: 1},
   "volume": {name: "volume", dimensionality: "L^3", unit: [3,0,0,0,0,0,0], value: 1},
   "volume per length": {name: "volume per length", dimensionality: "L^3/L", unit: [2,0,0,0,0,0,0], value: 1},
   "volume power density": {name: "volume power density", dimensionality: "L^2*M/(L^3*T^3)", unit: [-1,1,-3,0,0,0,0], value: 1},
   "volume ratio": {name: "volume ratio", dimensionality: "L^3/L^3", unit: [0,0,0,0,0,0,0], value: 1},
   "volumetric flow rate": {name: "volumetric flow rate", dimensionality: "L^3/T", unit: [3,0,-1,0,0,0,0], value: 1},
   "wavelength displacement constant": {name: "wavelength displacement constant", dimensionality: "L*Θ", unit: [1,0,0,0,1,0,0], value: 1},
   "wavenumber": {name: "wavenumber", dimensionality: "1/L", unit: [-1,0,0,0,0,0,0], value: 1}
}


Quantity.preferredUnits = ["A", "cd", "°", "H", "Hz", "J",  "K", "kg",  "m", "N", "Ω", "ppm", "Pa", "s", "S", "T", "V", "W", "Wb"];


Quantity.unitsConstants = {
   "ac": {name: "acres", symbol: "ac", si: "4046.8564224 m^2", value: 4046.8564224, unit: [2, 0, 0, 0, 0, 0, 0]},
   "m_a": {name: "alpha particle mass", symbol: "m_a", si: "6.64465723e-27 kg", value: 6.64465723e-27, unit: [0, 1, 0, 0, 0, 0, 0]},
   "A": {name: "amperes", symbol: "A", si: "1 A", value: 1, unit: [0, 0, 0, 1, 0, 0, 0]},
   "ua": {name: "astronomical units", symbol: "ua", si: "149597870691 m", value: 149597870691, unit: [1, 0, 0, 0, 0, 0, 0]},
   "atm": {name: "atmospheres", symbol: "atm", si: "101325 kg/(m*s^2)", value: 101325, unit: [-1, 1, -2, 0, 0, 0, 0]},
   "m_u": {name: "atomic mass constant", symbol: "m_u", si: "1.66053904e-27 kg", value: 1.66053904e-27, unit: [0, 1, 0, 0, 0, 0, 0]},
   "u": {name: "atomic mass units", symbol: "u", si: "1.66053904e-27 kg", value: 1.66053904e-27, unit: [0, 1, 0, 0, 0, 0, 0]},
   "Λ_0": {name: "atomic unit of electric field gradient", symbol: "Λ_0", si: "9.717362365265548e+21 m^2*kg/(m^2*s^3*A)", value: 9.717362365265548e+21, unit: [0, 1, -3, -1, 0, 0, 0]},
   "E_h": {name: "atomic unit of energy", symbol: "E_h", si: "4.359744651391459e-18 m^2*kg/s^2", value: 4.359744651391459e-18, unit: [2, 1, -2, 0, 0, 0, 0]},
   "a_0": {name: "atomic unit of length", symbol: "a_0", si: "5.29177210526763e-11 m", value: 5.29177210526763e-11, unit: [1, 0, 0, 0, 0, 0, 0]},
   "N_A": {name: "avogadro constant", symbol: "N_A", si: "6.022140857e+23 (1/mol)", value: 6.022140857e+23, unit: [0, 0, 0, 0, 0, -1, 0]},
   "bar": {name: "bars", symbol: "bar", si: "100000 kg/(m*s^2)", value: 100000, unit: [-1, 1, -2, 0, 0, 0, 0]},
   "b": {name: "barns", symbol: "b", si: "1e-28 m^2", value: 1e-28, unit: [2, 0, 0, 0, 0, 0, 0]},
   "Bq": {name: "becquerels", symbol: "Bq", si: "1 (1/s)", value: 1, unit: [0, 0, -1, 0, 0, 0, 0]},
   "µ_B": {name: "bohr magnetons", symbol: "µ_B", si: "9.274009992054043e-24 m^2*A", value: 9.274009992054043e-24, unit: [2, 0, 0, 1, 0, 0, 0]},
   "k_B": {name: "boltzmann constant", symbol: "k_B", si: "1.38064852e-23 m^2*kg/(s^2*K)", value: 1.38064852e-23, unit: [2, 1, -2, 0, -1, 0, 0]},
   "B": {name: "brewsters", symbol: "B", si: "1e-12 m*s^2/kg", value: 1e-12, unit: [1, -1, 2, 0, 0, 0, 0]},
   "Btu": {name: "british thermal units", symbol: "Btu", si: "1055.05585257348 m^2*kg/s^2", value: 1055.05585257348, unit: [2, 1, -2, 0, 0, 0, 0]},
   "cal": {name: "calories", symbol: "cal", si: "4.1868 m^2*kg/s^2", value: 4.1868, unit: [2, 1, -2, 0, 0, 0, 0]},
   "cd": {name: "candelas", symbol: "cd", si: "1 cd", value: 1, unit: [0, 0, 0, 0, 0, 0, 1]},
   "°C": {name: "celsius", symbol: "°C", si: "1 K", value: 1, unit: [0, 0, 0, 0, 1, 0, 0]},
   "hyr": {name: "centuries", symbol: "hyr", si: "3155760000 s", value: 3155760000, unit: [0, 0, 1, 0, 0, 0, 0]},
   "ch": {name: "chains", symbol: "ch", si: "20.1168 m", value: 20.1168, unit: [1, 0, 0, 0, 0, 0, 0]},
   "Z_0": {name: "characteristic impedance of vacuum", symbol: "Z_0", si: "376.7303134803528 m^2*kg/(s^3*A^2)", value: 376.7303134803528, unit: [2, 1, -3, -2, 0, 0, 0]},
   "λ_C": {name: "compton wavelengths", symbol: "λ_C", si: "2.426310235972943e-12 m", value: 2.426310235972943e-12, unit: [1, 0, 0, 0, 0, 0, 0]},
   "G_0": {name: "conductance quantum", symbol: "G_0", si: "7.748091730820603e-05 s^3*A^2/(m^2*kg)", value: 0.00007748091730820603, unit: [-2, -1, 3, 2, 0, 0, 0]},
   "C": {name: "coulombs", symbol: "C", si: "1 s*A", value: 1, unit: [0, 0, 1, 1, 0, 0, 0]},
   "cup": {name: "cups", symbol: "cup", si: "0.0002365882365 m^3", value: 0.0002365882365, unit: [3, 0, 0, 0, 0, 0, 0]},
   "Ci": {name: "curies", symbol: "Ci", si: "37000000000 (1/s)", value: 37000000000, unit: [0, 0, -1, 0, 0, 0, 0]},
   "Da": {name: "daltons", symbol: "Da", si: "1.66053904e-27 kg", value: 1.66053904e-27, unit: [0, 1, 0, 0, 0, 0, 0]},
   "Dc": {name: "darcys", symbol: "Dc", si: "9.869233e-13 m^2", value: 9.869233e-13, unit: [2, 0, 0, 0, 0, 0, 0]},
   "d": {name: "days", symbol: "d", si: "86400 s", value: 86400, unit: [0, 0, 1, 0, 0, 0, 0]},
   "D": {name: "debyes", symbol: "D", si: "3.335640951816991e-30 m*s*A", value: 3.335640951816991e-30, unit: [1, 0, 1, 1, 0, 0, 0]},
   "dayr": {name: "decades", symbol: "dayr", si: "315576000 s", value: 315576000, unit: [0, 0, 1, 0, 0, 0, 0]},
   "°": {name: "degrees", symbol: "°", si: "0.0174532925199433 m/m", value: 0.0174532925199433, unit: [0, 0, 0, 0, 0, 0, 0]},
   "dr": {name: "drams", symbol: "dr", si: "0.0017718451953125 kg", value: 0.0017718451953125, unit: [0, 1, 0, 0, 0, 0, 0]},
   "dyn": {name: "dynes", symbol: "dyn", si: "1e-05 m*kg/s^2", value: 0.00001, unit: [1, 1, -2, 0, 0, 0, 0]},
   "ε_0": {name: "electric constant", symbol: "ε_0", si: "8.854187817e-12 s^4*A^2/(m^3*kg)", value: 8.854187817e-12, unit: [-3, -1, 4, 2, 0, 0, 0]},
   "g_e": {name: "electron g factor", symbol: "g_e", si: "-2.00231930436182 m^2*A/(m^2*A)", value: -2.00231930436182, unit: [0, 0, 0, 0, 0, 0, 0]},
   "µ_e": {name: "electron magnetic moment", symbol: "µ_e", si: "-9.28476462e-24 m^2*A", value: -9.28476462e-24, unit: [2, 0, 0, 1, 0, 0, 0]},
   "m_e": {name: "electron mass", symbol: "m_e", si: "9.10938356e-31 kg", value: 9.10938356e-31, unit: [0, 1, 0, 0, 0, 0, 0]},
   "eV": {name: "electronvolts", symbol: "eV", si: "1.6021766208e-19 m^2*kg/s^2", value: 1.6021766208e-19, unit: [2, 1, -2, 0, 0, 0, 0]},
   "q_e": {name: "elementary charge", symbol: "q_e", si: "1.6021766208e-19 s*A", value: 1.6021766208e-19, unit: [0, 0, 1, 1, 0, 0, 0]},
   "erg": {name: "ergs", symbol: "erg", si: "1e-07 m^2*kg/s^2", value: 1e-7, unit: [2, 1, -2, 0, 0, 0, 0]},
   "e": {name: "euler constant", symbol: "e", si: "2.71828182845905", value: 2.71828182845905, unit: [0, 0, 0, 0, 0, 0, 0]},
   "°F": {name: "fahrenheit", symbol: "°F", si: "0.555555555555556 K", value: 0.555555555555556, unit: [0, 0, 0, 0, 1, 0, 0]},
   "F": {name: "farads", symbol: "F", si: "1 s^4*A^2/(m^2*kg)", value: 1, unit: [-2, -1, 4, 2, 0, 0, 0]},
   "&F": {name: "faraday constant", symbol: "&F", si: "96485.33288249877 s*A/mol", value: 96485.33288249877, unit: [0, 0, 1, 1, 0, -1, 0]},
   "ftm": {name: "fathoms", symbol: "ftm", si: "1.8288 m", value: 1.8288, unit: [1, 0, 0, 0, 0, 0, 0]},
   "α": {name: "fine structure constant", symbol: "α", si: "0.00729735256635786", value: 0.00729735256635786, unit: [0, 0, 0, 0, 0, 0, 0]},
   "floz": {name: "fluid ounces", symbol: "floz", si: "2.95735295625e-05 m^3", value: 0.0000295735295625, unit: [3, 0, 0, 0, 0, 0, 0]},
   "ft": {name: "feet", symbol: "ft", si: "0.3048 m", value: 0.3048, unit: [1, 0, 0, 0, 0, 0, 0]},
   "fur": {name: "furlongs", symbol: "fur", si: "201.168 m", value: 201.168, unit: [1, 0, 0, 0, 0, 0, 0]},
   "gal": {name: "gallons", symbol: "gal", si: "0.003785411784 m^3", value: 0.003785411784, unit: [3, 0, 0, 0, 0, 0, 0]},
   "R": {name: "gas constant", symbol: "R", si: "8.314459861448581 m^2*kg/(s^2*K*mol)", value: 8.314459861448581, unit: [2, 1, -2, 0, -1, -1, 0]},
   "GPU": {name: "gas permeance unit", symbol: "GPU", si: "0.33 m*s^2*mol/(m^2*kg*s)", value: 0.33, unit: [-1, -1, 1, 0, 0, 1, 0]},
   "G": {name: "gauss", symbol: "G", si: "0.0001 kg/(s^2*A)", value: 0.0001, unit: [0, 1, -2, -1, 0, 0, 0]},
   "gi": {name: "gill", symbol: "gi", si: "0.00011829411825 m^3", value: 0.00011829411825, unit: [3, 0, 0, 0, 0, 0, 0]},
   "gr": {name: "grains", symbol: "gr", si: "6.479891e-05 kg", value: 0.00006479891, unit: [0, 1, 0, 0, 0, 0, 0]},
   "kg": {name: "kilograms", symbol: "kg", si: "1 kg", value: 1, unit: [0, 1, 0, 0, 0, 0, 0]},
   "g": {name: "grams", symbol: "g", si: "0.001 kg", value: 0.001, unit: [0, 1, 0, 0, 0, 0, 0]},
   "&G": {name: "gravitational constant", symbol: "&G", si: "6.67408e-11 m^3/(kg*s^2)", value: 6.67408e-11, unit: [3, -1, -2, 0, 0, 0, 0]},
   "g_0": {name: "gravity acceleration", symbol: "g_0", si: "9.80665 m/s^2", value: 9.80665, unit: [1, 0, -2, 0, 0, 0, 0]},
   "Gy": {name: "grays", symbol: "Gy", si: "1 m^2*kg/(kg*s^2)", value: 1, unit: [2, 0, -2, 0, 0, 0, 0]},
   "halftsp": {name: "half teaspoons", symbol: "halftsp", si: "2.464460796875e-06 m^3", value: 0.000002464460796875, unit: [3, 0, 0, 0, 0, 0, 0]},
   "ha": {name: "hectares", symbol: "ha", si: "10000 m^2", value: 10000, unit: [2, 0, 0, 0, 0, 0, 0]},
   "H": {name: "henries", symbol: "H", si: "1 m^2*kg/(s^2*A^2)", value: 1, unit: [2, 1, -2, -2, 0, 0, 0]},
   "Hz": {name: "hertz", symbol: "Hz", si: "1 (1/s)", value: 1, unit: [0, 0, -1, 0, 0, 0, 0]},
   "hp": {name: "horsepower", symbol: "hp", si: "745.699872 m^2*kg/s^3", value: 745.699872, unit: [2, 1, -3, 0, 0, 0, 0]},
   "h": {name: "hours", symbol: "h", si: "3600 s", value: 3600, unit: [0, 0, 1, 0, 0, 0, 0]},
   "cwt": {name: "hundredweight", symbol: "cwt", si: "45.359237 kg", value: 45.359237, unit: [0, 1, 0, 0, 0, 0, 0]},
   "cwtUK": {name: "hundredweightUK", symbol: "cwtUK", si: "50.80234544 kg", value: 50.80234544, unit: [0, 1, 0, 0, 0, 0, 0]},
   "cupUK": {name: "imperial cups", symbol: "cupUK", si: "0.000284130625 m^3", value: 0.000284130625, unit: [3, 0, 0, 0, 0, 0, 0]},
   "flozUK": {name: "imperial fluid ounces", symbol: "flozUK", si: "2.84130625e-05 m^3", value: 0.0000284130625, unit: [3, 0, 0, 0, 0, 0, 0]},
   "galUK": {name: "imperial gallons", symbol: "galUK", si: "0.00454609 m^3", value: 0.00454609, unit: [3, 0, 0, 0, 0, 0, 0]},
   "giUK": {name: "imperial gill", symbol: "giUK", si: "0.0001420653125 m^3", value: 0.0001420653125, unit: [3, 0, 0, 0, 0, 0, 0]},
   "halftspUK": {name: "imperial half teaspoons", symbol: "halftspUK", si: "2.959694010416667e-06 m^3", value: 0.000002959694010416667, unit: [3, 0, 0, 0, 0, 0, 0]},
   "ptUK": {name: "imperial pints", symbol: "ptUK", si: "0.00056826125 m^3", value: 0.00056826125, unit: [3, 0, 0, 0, 0, 0, 0]},
   "qtUK": {name: "imperial quarts", symbol: "qtUK", si: "0.0011365225 m^3", value: 0.0011365225, unit: [3, 0, 0, 0, 0, 0, 0]},
   "quartertspUK": {name: "imperial quarter teaspoons", symbol: "quartertspUK", si: "1.479847005208333e-06 m^3", value: 0.000001479847005208333, unit: [3, 0, 0, 0, 0, 0, 0]},
   "tbspUK": {name: "imperial tablespoons", symbol: "tbspUK", si: "1.77581640625e-05 m^3", value: 0.0000177581640625, unit: [3, 0, 0, 0, 0, 0, 0]},
   "tspUK": {name: "imperial teaspoons", symbol: "tspUK", si: "5.919388020833334e-06 m^3", value: 0.000005919388020833334, unit: [3, 0, 0, 0, 0, 0, 0]},
   "in": {name: "inches", symbol: "in", si: "0.0254 m", value: 0.0254, unit: [1, 0, 0, 0, 0, 0, 0]},
   "J": {name: "joules", symbol: "J", si: "1 m^2*kg/s^2", value: 1, unit: [2, 1, -2, 0, 0, 0, 0]},
   "kat": {name: "katals", symbol: "kat", si: "1 mol/s", value: 1, unit: [0, 0, -1, 0, 0, 1, 0]},
   "K": {name: "kelvin", symbol: "K", si: "1 K", value: 1, unit: [0, 0, 0, 0, 1, 0, 0]},
   "kcal": {name: "kilocalories", symbol: "kcal", si: "4186.8 m^2*kg/s^2", value: 4186.8, unit: [2, 1, -2, 0, 0, 0, 0]},
   "kgf": {name: "kilograms force", symbol: "kgf", si: "9.80665 m*kg/s^2", value: 9.80665, unit: [1, 1, -2, 0, 0, 0, 0]},
   "kn": {name: "knots", symbol: "kn", si: "0.514444444444444 m/s", value: 0.514444444444444, unit: [1, 0, -1, 0, 0, 0, 0]},
   "lea": {name: "leagues", symbol: "lea", si: "4828.032 m", value: 4828.032, unit: [1, 0, 0, 0, 0, 0, 0]},
   "ly": {name: "light years", symbol: "ly", si: "9460730473047448 m", value: 9460730473047448, unit: [1, 0, 0, 0, 0, 0, 0]},
   "li": {name: "links", symbol: "li", si: "0.201168 m", value: 0.201168, unit: [1, 0, 0, 0, 0, 0, 0]},
   "L": {name: "liters", symbol: "L", si: "0.001 m^3", value: 0.001, unit: [3, 0, 0, 0, 0, 0, 0]},
   "lm": {name: "lumens", symbol: "lm", si: "1 m^2*cd/m^2", value: 1, unit: [0, 0, 0, 0, 0, 0, 1]},
   "lx": {name: "lux", symbol: "lx", si: "1 m^2*cd/m^4", value: 1, unit: [-2, 0, 0, 0, 0, 0, 1]},
   "µ_0": {name: "magnetic constant", symbol: "µ_0", si: "1.256637061435917e-06 m*kg/(s^2*A^2)", value: 0.000001256637061435917, unit: [1, 1, -2, -2, 0, 0, 0]},
   "Φ_0": {name: "magnetic flux quantum", symbol: "Φ_0", si: "2.067832279214686e-15 m^2*kg/(s^2*A)", value: 2.067832279214686e-15, unit: [2, 1, -2, -1, 0, 0, 0]},
   "Mx": {name: "maxwells", symbol: "Mx", si: "1e-08 m^2*kg/(s^2*A)", value: 1e-8, unit: [2, 1, -2, -1, 0, 0, 0]},
   "m": {name: "meters", symbol: "m", si: "1 m", value: 1, unit: [1, 0, 0, 0, 0, 0, 0]},
   "mi": {name: "miles", symbol: "mi", si: "1609.344 m", value: 1609.344, unit: [1, 0, 0, 0, 0, 0, 0]},
   "mmHg": {name: "millimeters of Hg", symbol: "mmHg", si: "133.322 kg/(m*s^2)", value: 133.322, unit: [-1, 1, -2, 0, 0, 0, 0]},
   "min": {name: "minutes", symbol: "min", si: "60 s", value: 60, unit: [0, 0, 1, 0, 0, 0, 0]},
   "mol": {name: "moles", symbol: "mol", si: "1 mol", value: 1, unit: [0, 0, 0, 0, 0, 1, 0]},
   "M": {name: "moles per liter", symbol: "M", si: "1000 mol/m^3", value: 1000, unit: [-3, 0, 0, 0, 0, 1, 0]},
   "month": {name: "months", symbol: "month", si: "2629800 s", value: 2629800, unit: [0, 0, 1, 0, 0, 0, 0]},
   "g_µ": {name: "muon g factor", symbol: "g_µ", si: "-2.0023318418 m^2*A/(m^2*A)", value: -2.0023318418, unit: [0, 0, 0, 0, 0, 0, 0]},
   "µ_µ": {name: "muon magnetic moment", symbol: "µ_µ", si: "-4.49044826e-26 m^2*A", value: -4.49044826e-26, unit: [2, 0, 0, 1, 0, 0, 0]},
   "m_µ": {name: "myon mass", symbol: "m_µ", si: "1.883531594e-28 kg", value: 1.883531594e-28, unit: [0, 1, 0, 0, 0, 0, 0]},
   "g_n": {name: "neutron g factor", symbol: "g_n", si: "-3.82608545 m^2*A/(m^2*A)", value: -3.82608545, unit: [0, 0, 0, 0, 0, 0, 0]},
   "µ_n": {name: "neutron magnetic moment", symbol: "µ_n", si: "-9.662365e-27 m^2*A", value: -9.662365e-27, unit: [2, 0, 0, 1, 0, 0, 0]},
   "m_n": {name: "neutron mass", symbol: "m_n", si: "1.674927471e-27 kg", value: 1.674927471e-27, unit: [0, 1, 0, 0, 0, 0, 0]},
   "N": {name: "newtons", symbol: "N", si: "1 m*kg/s^2", value: 1, unit: [1, 1, -2, 0, 0, 0, 0]},
   "µ_N": {name: "nuclear magnetons", symbol: "µ_N", si: "5.050783698211084e-27 m^2*A", value: 5.050783698211084e-27, unit: [2, 0, 0, 1, 0, 0, 0]},
   "Ω": {name: "ohms", symbol: "Ω", si: "1 m^2*kg/(s^3*A^2)", value: 1, unit: [2, 1, -3, -2, 0, 0, 0]},
   "bbl": {name: "oil barrels", symbol: "bbl", si: "0.158987295 m^3", value: 0.158987295, unit: [3, 0, 0, 0, 0, 0, 0]},
   "oz": {name: "ounces", symbol: "oz", si: "0.028349523125 kg", value: 0.028349523125, unit: [0, 1, 0, 0, 0, 0, 0]},
   "ozf": {name: "ounces force", symbol: "ozf", si: "0.2780138509537812 m*kg/s^2", value: 0.2780138509537812, unit: [1, 1, -2, 0, 0, 0, 0]},
   "ppb": {name: "parts per billion", symbol: "ppb", si: "1E-09", value: 1e-9, unit: [0, 0, 0, 0, 0, 0, 0]},
   "ppm": {name: "parts per million", symbol: "ppm", si: "1E-06", value: 0.000001, unit: [0, 0, 0, 0, 0, 0, 0]},
   "ppq": {name: "parts per quadrillion", symbol: "ppq", si: "1E-15", value: 1e-15, unit: [0, 0, 0, 0, 0, 0, 0]},
   "‱": {name: "parts per ten thousand", symbol: "‱", si: "0.0001", value: 0.0001, unit: [0, 0, 0, 0, 0, 0, 0]},
   "‰": {name: "parts per thousand", symbol: "‰", si: "0.001", value: 0.001, unit: [0, 0, 0, 0, 0, 0, 0]},
   "ppt": {name: "parts per trillion", symbol: "ppt", si: "1E-12", value: 1e-12, unit: [0, 0, 0, 0, 0, 0, 0]},
   "Pa": {name: "pascals", symbol: "Pa", si: "1 kg/(m*s^2)", value: 1, unit: [-1, 1, -2, 0, 0, 0, 0]},
   "%": {name: "percent", symbol: "%", si: "0.01", value: 0.01, unit: [0, 0, 0, 0, 0, 0, 0]},
   "ph": {name: "phots", symbol: "ph", si: "10000 m^2*cd/m^4", value: 10000, unit: [-2, 0, 0, 0, 0, 0, 1]},
   "π": {name: "pi", symbol: "π", si: "3.141592653589793 m/m", value: 3.141592653589793, unit: [0, 0, 0, 0, 0, 0, 0]},
   "pt": {name: "pints", symbol: "pt", si: "0.000473176473 m^3", value: 0.000473176473, unit: [3, 0, 0, 0, 0, 0, 0]},
   "h_P": {name: "planck constant", symbol: "h_P", si: "6.62607004e-34 m^2*kg/s", value: 6.62607004e-34, unit: [2, 1, -1, 0, 0, 0, 0]},
   "l_P": {name: "planck length", symbol: "l_P", si: "1.616228372961306e-35 m", value: 1.616228372961306e-35, unit: [1, 0, 0, 0, 0, 0, 0]},
   "m_P": {name: "planck mass", symbol: "m_P", si: "2.176470195687873e-08 kg", value: 2.176470195687873e-8, unit: [0, 1, 0, 0, 0, 0, 0]},
   "T_P": {name: "planck temperature", symbol: "T_P", si: "1.416807993922871e+32 K", value: 1.416807993922871e+32, unit: [0, 0, 0, 0, 1, 0, 0]},
   "t_P": {name: "planck time", symbol: "t_P", si: "5.391157548338278e-44 s", value: 5.391157548338278e-44, unit: [0, 0, 1, 0, 0, 0, 0]},
   "P": {name: "poises", symbol: "P", si: "0.1 kg/(m*s)", value: 0.1, unit: [-1, 1, -1, 0, 0, 0, 0]},
   "lb": {name: "pounds", symbol: "lb", si: "0.45359237 kg", value: 0.45359237, unit: [0, 1, 0, 0, 0, 0, 0]},
   "lbf": {name: "pounds force", symbol: "lbf", si: "4.4482216152605 m*kg/s^2", value: 4.4482216152605, unit: [1, 1, -2, 0, 0, 0, 0]},
   "psi": {name: "pounds force per square inch", symbol: "psi", si: "6894.75729 kg/(m*s^2)", value: 6894.75729, unit: [-1, 1, -2, 0, 0, 0, 0]},
   "g_p": {name: "proton g factor", symbol: "g_p", si: "5.585694702 m^2*A/(m^2*A)", value: 5.585694702, unit: [0, 0, 0, 0, 0, 0, 0]},
   "µ_p": {name: "proton magnetic moment", symbol: "µ_p", si: "1.4106067873e-26 m^2*A", value: 1.4106067873e-26, unit: [2, 0, 0, 1, 0, 0, 0]},
   "m_p": {name: "proton mass", symbol: "m_p", si: "1.672621898e-27 kg", value: 1.672621898e-27, unit: [0, 1, 0, 0, 0, 0, 0]},
   "qt": {name: "quarts", symbol: "qt", si: "0.000946352946 m^3", value: 0.000946352946, unit: [3, 0, 0, 0, 0, 0, 0]},
   "quartertsp": {name: "quarter teaspoons", symbol: "quartertsp", si: "1.2322303984375e-06 m^3", value: 0.0000012322303984375, unit: [3, 0, 0, 0, 0, 0, 0]},
   "rad": {name: "radians", symbol: "rad", si: "1 m/m", value: 1, unit: [0, 0, 0, 0, 0, 0, 0]},
   "°R": {name: "rankines", symbol: "°R", si: "0.555555555555556 K", value: 0.555555555555556, unit: [0, 0, 0, 0, 1, 0, 0]},
   "ℏ": {name: "reduced planck constant", symbol: "ℏ", si: "1.054571800139113e-34 m^3*kg/(m*s)", value: 1.054571800139113e-34, unit: [2, 1, -1, 0, 0, 0, 0]},
   "rod": {name: "rods", symbol: "rod", si: "5.0292 m", value: 5.0292, unit: [1, 0, 0, 0, 0, 0, 0]},
   "Ry": {name: "rydbergs", symbol: "Ry", si: "2.179872325695729e-18 m^2*kg/s^2", value: 2.179872325695729e-18, unit: [2, 1, -2, 0, 0, 0, 0]},
   "R_∞": {name: "rydberg constant", symbol: "R_∞", si: "10973731.57154738 (1/m)", value: 10973731.57154738, unit: [-1, 0, 0, 0, 0, 0, 0]},
   "s": {name: "seconds", symbol: "s", si: "1 s", value: 1, unit: [0, 0, 1, 0, 0, 0, 0]},
   "S": {name: "siemens", symbol: "S", si: "1 s^3*A^2/(m^2*kg)", value: 1, unit: [-2, -1, 3, 2, 0, 0, 0]},
   "Sv": {name: "sieverts", symbol: "Sv", si: "1 m^2*kg/(kg*s^2)", value: 1, unit: [2, 0, -2, 0, 0, 0, 0]},
   "c_0": {name: "speed of light", symbol: "c_0", si: "299792458.0147872 m/s", value: 299792458.0147872, unit: [1, 0, -1, 0, 0, 0, 0]},
   "σ": {name: "stefan-boltzmann constant", symbol: "σ", si: "5.670367e-08 kg/(s^3*K^4)", value: 5.670367e-8, unit: [0, 1, -3, 0, -4, 0, 0]},
   "sr": {name: "steradians", symbol: "sr", si: "1 m^2/m^2", value: 1, unit: [0, 0, 0, 0, 0, 0, 0]},
   "sb": {name: "stilbs", symbol: "sb", si: "10000 cd/m^2", value: 10000, unit: [-2, 0, 0, 0, 0, 0, 1]},
   "St": {name: "stokes", symbol: "St", si: "0.0001 m^2/s", value: 0.0001, unit: [2, 0, -1, 0, 0, 0, 0]},
   "st": {name: "stones", symbol: "st", si: "6.35029318 kg", value: 6.35029318, unit: [0, 1, 0, 0, 0, 0, 0]},
   "tbsp": {name: "tablespoons", symbol: "tbsp", si: "1.478676478125e-05 m^3", value: 0.00001478676478125, unit: [3, 0, 0, 0, 0, 0, 0]},
   "tsp": {name: "teaspoons", symbol: "tsp", si: "4.92892159375e-06 m^3", value: 0.00000492892159375, unit: [3, 0, 0, 0, 0, 0, 0]},
   "T": {name: "tesla", symbol: "T", si: "1 kg/(s^2*A)", value: 1, unit: [0, 1, -2, -1, 0, 0, 0]},
   "Th": {name: "thomson", symbol: "Th", si: "1.036426957204542e-08 kg/(s*A)", value: 1.036426957204542e-8, unit: [0, 1, -1, -1, 0, 0, 0]},
   "ton": {name: "tons", symbol: "ton", si: "907.18474 kg", value: 907.18474, unit: [0, 1, 0, 0, 0, 0, 0]},
   "t": {name: "tonnes", symbol: "t", si: "1000 kg", value: 1000, unit: [0, 1, 0, 0, 0, 0, 0]},
   "tonUK": {name: "tonsUK", symbol: "tonUK", si: "1016.0469088 kg", value: 1016.0469088, unit: [0, 1, 0, 0, 0, 0, 0]},
   "Torr": {name: "torrs", symbol: "Torr", si: "133.3223684210526 kg/(m*s^2)", value: 133.3223684210526, unit: [-1, 1, -2, 0, 0, 0, 0]},
   "twp": {name: "townships", symbol: "twp", si: "93239571.972096 m^2", value: 93239571.972096, unit: [2, 0, 0, 0, 0, 0, 0]},
   "V": {name: "volts", symbol: "V", si: "1 m^2*kg/(s^3*A)", value: 1, unit: [2, 1, -3, -1, 0, 0, 0]},
   "W": {name: "watts", symbol: "W", si: "1 m^2*kg/s^3", value: 1, unit: [2, 1, -3, 0, 0, 0, 0]},
   "Wb": {name: "webers", symbol: "Wb", si: "1 m^2*kg/(s^2*A)", value: 1, unit: [2, 1, -2, -1, 0, 0, 0]},
   "wk": {name: "weeks", symbol: "wk", si: "604800 s", value: 604800, unit: [0, 0, 1, 0, 0, 0, 0]},
   "b_λ": {name: "wien wavelength displacement constant", symbol: "b_λ", si: "0.0028977729 m*K", value: 0.0028977729, unit: [1, 0, 0, 0, 1, 0, 0]},
   "yd": {name: "yards", symbol: "yd", si: "0.9144 m", value: 0.9144, unit: [1, 0, 0, 0, 0, 0, 0]},
   "yr": {name: "years", symbol: "yr", si: "31557600 s", value: 31557600, unit: [0, 0, 1, 0, 0, 0, 0]},
   "Å": {name: "ångströms", symbol: "Å", si: "1e-10 m", value: 1e-10, unit: [1, 0, 0, 0, 0, 0, 0]},
   "Oe": {name: "ørsteds", symbol: "Oe", si: "79.57747154594767 A/m", value: 79.57747154594767, unit: [-1, 0, 0, 1, 0, 0, 0]}
}

Quantity.reciprocal = {
   "amount": "inverse amount",
   "amount concentration": "molar magnetic susceptibility",
   "amount ratio": "amount ratio",
   "angular frequency": "time",
   "angular speed": "time",
   "angular velocity": "time",
   "area": "inverse area",
   "area ratio": "area ratio",
   "charge to mass ratio": "mass to charge ratio",
   "compressibility": "elastic modulus",
   "current": "inverse current",
   "current ratio": "current ratio",
   "density": "specific volume",
   "dimensionless": "dimensionless",
   "distance per volume": "volume per length",
   "dynamic viscosity": "fluidity",
   "elastic modulus": "compressibility",
   "electric conductance": "electric resistance",
   "electric conductivity": "electric resistivity",
   "electric resistance": "electric conductance",
   "electric resistivity": "electric conductivity",
   "electrical mobility": "magnetic flux density",
   "energy density": "compressibility",
   "fine structure constant": "fine structure constant",
   "fluidity": "dynamic viscosity",
   "frequency": "time",
   "frequency per electric field gradient": "magnetic flux density",
   "frequency per magnetic flux density": "mass to charge ratio",
   "frequency ratio": "frequency ratio",
   "gyromagnetic ratio": "mass to charge ratio",
   "inverse amount": "amount",
   "inverse area": "area",
   "inverse current": "current",
   "inverse length": "length",
   "inverse luminous intensity": "luminous intensity",
   "inverse magnetic flux density": "magnetic flux density",
   "inverse mass": "mass",
   "inverse temperature": "temperature",
   "inverse time": "time",
   "inverse volume": "volume",
   "length": "inverse length",
   "length ratio": "length ratio",
   "luminous efficacy": "power per luminous flux",
   "luminous flux": "inverse luminous intensity",
   "luminous intensity": "inverse luminous intensity",
   "luminous intensity ratio": "luminous intensity ratio",
   "magnetic dipole moment ratio": "magnetic dipole moment ratio",
   "magnetic flux density": "electrical mobility",
   "magnetic flux density": "inverse magnetic flux density",
   "mass": "inverse mass",
   "mass concentration": "specific volume",
   "mass ratio": "mass ratio",
   "mass to charge ratio": "charge to mass ratio",
   "molality": "molar mass",
   "molar magnetic susceptibility": "amount concentration",
   "molar mass": "molality",
   "porosity": "porosity",
   "power per luminous flux": "luminous efficacy",
   "pressure": "compressibility",
   "radiation exposure": "mass to charge ratio",
   "radioactivity": "time",
   "refractive index": "refractive index",
   "solid angle": "solid angle",
   "specific gravity": "specific gravity",
   "specific surface area": "surface density",
   "specific volume": "density",
   "specific volume": "mass concentration",
   "stress": "compressibility",
   "stress-optic coefficient": "stress",
   "surface area to volume ratio": "length",
   "surface density": "specific surface area",
   "temperature": "inverse temperature",
   "temperature ratio": "temperature ratio",
   "time": "frequency",
   "time ratio": "time ratio",
   "volume": "inverse volume",
   "volume per length": "distance per volume",
   "volume per length": "inverse area",
   "volume ratio": "volume ratio",
   "wavenumber": "length"
}